# cicd-go-container
This is a demo of a web-server written in go. It will be run on a command line, as a container and using a CI/CD-pipeline.
As codebase, CI/CD-pipeline and container registry GitLab will be used. The deployment of the container will be on a K3s-Kubernetes cluster.
(The resulting container is registered to gitlab/kweronek/registry.)

## Prerequisites
#### installations
| requirement | check | if not see |
| ------ | ------ | ------|
| `git` | `git version` |  https://git-scm.com/book/en/v2/Getting-Started-Installing-Git  |
| `go` | `go version` |  https://golang.org/doc/install  |
| `docker` | `docker version` |  https://docs.docker.com/get-docker/  |
#### configurations
```
mkdir helloServer
cd helloServer
git clone https://gitlab.com/kweronek/cicd-go-container .
go get github.com/gorilla/mux
```
## Run and kill server on command line

#### run the server
```
go run main.go &
```
#### test the server
The unit test of the server will be in a terminal: `go test`. The test is in `main_test.co` .  
The user test is a browser: 
```
http://localhost:8080/greeting
```  
or with `curl http://localhost:8080/greeting` in a command line .  

You will get `"Hello, World!"` .  
(Note: the server uses `port :8080` .)  
#### stop the server
```
kill $(pidof main)
```
## Build and run and stop a container containing the server
#### build the container
To build and tag an image we use Docker. The build is configured in `Dockerfile?.  
Open a terminal and build the container using:
```
docker build . -t hello-container
```
Check the existance of the image by using `docker images`.
#### run the container
To run, publish and name the container, use:
```
docker run --publish 8000:8080 --name helloWorld hello-container &  
```
Check if the container is really running by using `docker ps`.  
(Note: The server uses `port :8080` in the container, but will be published on `port :8000` outside the container).  
#### test the container
To test in a browser use: 
```
http://localhost:8000/greeting
```
or with: `curl http://localhost:8000/greeting` in a terminal.  
#### stop the container
```
docker stop helloWorld
``` 
#### remove the image
```
docker rmi helloWorld
``` 

