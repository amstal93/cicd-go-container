# Stage 1 is called build
# go 1.15.2 was released 2020/08/11
FROM golang:1.15 as build

# prepare build dir and copy
RUN mkdir /build/
WORKDIR /build
COPY . /build/

# Gorilla mux is used in the example and has to be present
RUN go get github.com/gorilla/mux

# The build is here:
# RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o main
RUN GOOS=linux go build -ldflags '-extldflags "-static"' -o main

# Stage 2 is called execute
FROM golang as execute
# when the container is startet on the runtime environment the server will be started automatically
COPY --from=build /build/main /app/
WORKDIR /app
# start app by /app/main 
ENTRYPOINT ["./main"]
